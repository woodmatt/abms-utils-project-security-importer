﻿using Amec.ABMS.Common.ResultObjects;
using Amec.ABMS.DataServices.Dto.Security;
using Amec.ABMS.DataServices.Hibernate.Repositories;
using Amec.ABMS.DataServices.RepositoryContracts;
using Amec.ABMS.FluentHibernate.Session;
using ClosedXML.Excel;
using NLog;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms; 

namespace Amec.ABMS.AdminModule.ProjectSecurityImporter
{
    public partial class FrmImport : Form
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Form events
        public FrmImport()
        {
            InitializeComponent();
            logger.Debug("Logger Working");

            openFileDialog1.DereferenceLinks = false;
            openFileDialog1.Filter = "Excel files (*.xls or .xlsx)|.xls;*.xlsx";
        }

        private String _fileName; 

        private void btnSelectFile_Click(object sender, EventArgs e)
        {            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FileName.EndsWith(".xls") || openFileDialog1.FileName.EndsWith(".xlsx"))
                {
                    _fileName = openFileDialog1.FileName;

                    lblMessage.Text = "Selected : " + _fileName;

                    logger.Info("Selected : " + _fileName);
                    btnImport.Visible = true;                         
                }                
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_fileName))
            {
                return; 
            }

            try
            {
                var settings = new SecurityImportSettings();
                settings.RemoveCurrentSecurityBeforeImport = chkDeleteCurrentSettings.Checked;

                var workbook = new XLWorkbook(_fileName);
                var WorkbookProcessor = new WorkbookProcessor();
                var zeroErrors = WorkbookProcessor.ProcessWorkbook(workbook, settings);

                if (zeroErrors) { 
                    lblMessage.Text = "Successfully uploaded user project security." + Environment.NewLine + "The output has been written to the log file should you wish to examine it.";
                }
                else {
                    lblMessage.Text = "Processed user project security, but there were some issues." + Environment.NewLine + "Please review the log file for details.";
                }

                logger.Info("Import completed.");                
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message; 
                logger.Error(ex); 
            }

            btnViewLog.Visible = true; 
        }

        private void btnViewLog_Click(object sender, EventArgs e)
        {
            var directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var file = Path.Combine(directory, "ProjectSecurityImport.Log");
            Process.Start(file);
        }

        #endregion

    }
}
