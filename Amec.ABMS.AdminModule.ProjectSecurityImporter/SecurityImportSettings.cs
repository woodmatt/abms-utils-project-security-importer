﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amec.ABMS.AdminModule.ProjectSecurityImporter
{
    public class SecurityImportSettings
    {
        public Boolean RemoveCurrentSecurityBeforeImport { get; set; }
    }
}
