﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amec.ABMS.AdminModule.ProjectSecurityImporter
{
    public class UserSecurityRole
    {
        public int UserId { get; set; }
        public int WorkOrderId { get; set; }
        public int SecurityRoleId { get; set; }

        public UserSecurityRole(int userId, int workOrderId, int securityRoleId)
        {
            UserId = userId;
            WorkOrderId = workOrderId;
            SecurityRoleId = securityRoleId; 
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            UserSecurityRole p = obj as UserSecurityRole;
            if ((System.Object)p == null)
            {
                return false;
            }

            return UserId == p.UserId && WorkOrderId == p.WorkOrderId && SecurityRoleId == p.SecurityRoleId;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + UserId.GetHashCode();
                hash = hash * 23 + WorkOrderId.GetHashCode();
                hash = hash * 23 + SecurityRoleId.GetHashCode();
                return hash;
            }
        }


    }
}
