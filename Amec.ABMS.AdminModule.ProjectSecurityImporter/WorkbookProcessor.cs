﻿using Amec.ABMS.Common.ResultObjects;
using Amec.ABMS.DataServices.Dto.Security;
using Amec.ABMS.DataServices.Hibernate.Repositories;
using Amec.ABMS.DataServices.RepositoryContracts;
using Amec.ABMS.FluentHibernate.Session;
using ClosedXML.Excel;
using NLog;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Amec.ABMS.AdminModule.ProjectSecurityImporter
{
    public class WorkbookProcessor
    {

        #region DB Interaction
 
        public static string GetAdminModuleConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["AdminModule"].ConnectionString;
        }

        public List<UserSecurityRole> BulkUploadUserSecurityRolesForProject(List<UserSecurityRole> userSecurityRoles, Boolean deleteCurrentProjectSecurity)
        {
            var oracleConnection = new OracleConnection();
            oracleConnection.ConnectionString = GetAdminModuleConnectionString();

            if (userSecurityRoles != null && userSecurityRoles.Count > 0)
            {
                var workOrderId = userSecurityRoles[0].WorkOrderId;

                try
                {

                    string query = @"Insert into USER_SECURITY_ROLES " +
                    "(USER_ID, AWBS_WORK_ORDER_ID, SECURITY_ROLE_ID ) " +
                    " Values " +
                    " (:USER_ID, :AWBS_WORK_ORDER_ID, :SECURITY_ROLE_ID)";

                    oracleConnection.Open();
                    using (var command = oracleConnection.CreateCommand())
                    {
                        if (deleteCurrentProjectSecurity)
                        {
                            DeleteAllUserRolesForProject(workOrderId, command);
                        }
                        else
                        {
                            //Remove existing roles from the list to avoid duplication
                            userSecurityRoles = RemoveExistingUserSecurityRoles(userSecurityRoles, workOrderId, command);
                        }

                        if (userSecurityRoles.Count > 0)
                        {
                            command.CommandText = query;

                            command.ArrayBindCount = userSecurityRoles.Count;
                            command.Parameters.Add(":USER_ID", OracleDbType.Int32, userSecurityRoles.Select(c => c.UserId).ToArray(), ParameterDirection.Input);
                            command.Parameters.Add(":AWBS_WORK_ORDER_ID", OracleDbType.Int32, userSecurityRoles.Select(c => c.WorkOrderId).ToArray(), ParameterDirection.Input);
                            command.Parameters.Add(":SECURITY_ROLE_ID", OracleDbType.Int32, userSecurityRoles.Select(c => c.SecurityRoleId).ToArray(), ParameterDirection.Input);

                            int result = command.ExecuteNonQuery();
                        }
                    }
                }
                finally
                {
                    oracleConnection.Close();
                }
            }

            return userSecurityRoles;
        }

        private List<UserSecurityRole> RemoveExistingUserSecurityRoles(List<UserSecurityRole> userSecurityRoles, int projectId, OracleCommand command)
        {
            command.CommandText = "SELECT USER_ID, SECURITY_ROLE_ID FROM USER_SECURITY_ROLES WHERE AWBS_WORK_ORDER_ID = :AWBS_WORK_ORDER_ID";

            command.CommandType = CommandType.Text;
            command.BindByName = true;

            command.Parameters.Add(":AWBS_WORK_ORDER_ID", OracleDbType.Varchar2, projectId, ParameterDirection.Input);

            var reader = command.ExecuteReader();

            var existingRoles = reader.Cast<DbDataRecord>().Select(row =>
                    new UserSecurityRole(
                        Convert.ToInt32(row[0]),
                        projectId,
                        Convert.ToInt32(row[1])
                    )
                ).ToList();

            command.Parameters.Clear();

            return userSecurityRoles.Except(existingRoles).ToList();
        }

        private static void DeleteAllUserRolesForProject(int projectId, OracleCommand command)
        {
            command.CommandText = @"DELETE FROM USER_SECURITY_ROLES WHERE AWBS_WORK_ORDER_ID = :AWBS_WORK_ORDER_ID";

            command.CommandType = CommandType.Text;
            command.BindByName = true;

            command.Parameters.Add(":AWBS_WORK_ORDER_ID", OracleDbType.Varchar2, projectId, ParameterDirection.Input);

            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }

        private Dictionary<string, int> GetLookupFromSQL(string sql)
        {
            var oracleConnection = new OracleConnection();
            oracleConnection.ConnectionString = GetAdminModuleConnectionString();

            try
            {
                oracleConnection.Open();
                using (var command = oracleConnection.CreateCommand())
                {
                    command.CommandText = sql;
                    var reader = command.ExecuteReader();

                    return reader.Cast<DbDataRecord>().ToDictionary(row => (string)row[0], row => Convert.ToInt32(row[1]));
                }
            }
            finally
            {
                oracleConnection.Close();
            }
        }

        #endregion

        #region Logging
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private void LogError(string projectCode, int rowNumber, string errorMessage)
        {
            _zeroErrors = false;
            logger.Error("Tab: " + projectCode + " Row: " + rowNumber + " - " + errorMessage);
        }

        #endregion

        #region Spreadsheet Processing

        private const int USER_NAME_COLUMN_INDEX = 1;
        private const int USER_FORENAME_COLUMN_INDEX = 2;
        private const int USER_SURNAME_COLUMN_INDEX = 3;
        private const int USER_EMAIL_COLUMN_INDEX = 4;
        private const int FIRST_ROLE_COLUMN_INDEX = 8;

        private HSession _securitySession = HSessionFactory.CreateSession(Assembly.GetAssembly(typeof(BaseModel)), GetAdminModuleConnectionString());
        private IUserRepository _userRepository;
        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new SecurityModel(_securitySession)); }
        }

        //Get known entities from Admin Module
        private Dictionary<string, int> _projectLookup;
        private Dictionary<string, int> _userLookup;
        private Dictionary<string, int> _roleLookup;

        private Boolean _zeroErrors = true; 

        private Dictionary<string, int> GetProjectLookupFromAdminModule()
        {                        
            return GetLookupFromSQL(@"SELECT UPPER(WORK_ORDER), WORK_ORDER_ID FROM WORK_ORDERS");
        }

        private Dictionary<string, int> GetRoleLookupFromAdminModule()
        {
            return GetLookupFromSQL(@"SELECT UPPER(SECURITY_ROLE_NAME), SECURITY_ROLE_ID FROM SECURITY_ROLES");
        }

        private Dictionary<string, int> GetUserLookupFromAdminModule()
        {
            return GetLookupFromSQL(@"SELECT UPPER(USERNAME), USER_ID FROM USERS");
        }

        public Boolean ProcessWorkbook(XLWorkbook workbook, SecurityImportSettings settings)
        {
            ValidateProcessWorkbookParameters(workbook, settings);

            _projectLookup = GetProjectLookupFromAdminModule();
            _userLookup = GetUserLookupFromAdminModule();
            _roleLookup = GetRoleLookupFromAdminModule();

            foreach (var ws in workbook.Worksheets)
            {
                ProcessProjectWorkSheet(ws, settings);
            }

            return _zeroErrors; 
        }

        private static void ValidateProcessWorkbookParameters(XLWorkbook workbook, SecurityImportSettings settings)
        {
            if (workbook == null)
            {
                throw new ArgumentNullException("workbook");
            }

            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
        }

        private void ProcessProjectWorkSheet(IXLWorksheet ws, SecurityImportSettings settings)
        {
            List<UserSecurityRole> userSecurityRoles = new List<UserSecurityRole>();

            var projectCode = ws.Name.ToUpper();

            logger.Info("Tab: " + projectCode + " - Import started.");

            if (_projectLookup.ContainsKey(projectCode))
            {
                var projectId = _projectLookup[projectCode];

                var columnRoleIds = GetRoleIdsForRoleColumns(ws);

                ProcessUserRows(userSecurityRoles, ws, projectId, columnRoleIds);
                userSecurityRoles = BulkUploadUserSecurityRolesForProject(userSecurityRoles, settings.RemoveCurrentSecurityBeforeImport);

                logger.Info("Tab: " + projectCode + " - " + userSecurityRoles.Count + " new roles created");
            }
            else
            {
                LogError(projectCode, 0, "Project Code could not be found.");
            }
            logger.Info("Tab: " + projectCode + " - Import complete.");

        }

        private void ProcessUserRows(List<UserSecurityRole> userSecurityRoles, IXLWorksheet ws, int projectId, int?[] columnRoleIds)
        {
            // Loop the users 
            var processedUserIds = new HashSet<int>(); 

            foreach (var row in ws.Rows())
            {
                if (row.RowNumber() > 1)
                {
                    ProcessUserRow(userSecurityRoles, processedUserIds, projectId, columnRoleIds, row);
                }
            }
        }

        private void ProcessUserRow(List<UserSecurityRole> userSecurityRoles, HashSet<int> processedUserIds, int projectId, int?[] columnRoleIds, IXLRow row)
        {
            if (!row.Cell(USER_NAME_COLUMN_INDEX).IsEmpty())
            {
                var userName = row.Cell(USER_NAME_COLUMN_INDEX).Value.ToString().ToUpper();

                int userId = GetUserIdFromUserName(row, userName);
                if (userId > 0)
                {
                    if (!processedUserIds.Contains(userId))
                    {
                        for (int x = 0; x < columnRoleIds.Length; x++)
                        {
                            if (!row.Cell(x + FIRST_ROLE_COLUMN_INDEX).IsEmpty() && columnRoleIds[x].HasValue)
                            {
                                var roleId = columnRoleIds[x].Value;
                                userSecurityRoles.Add(new UserSecurityRole(userId, projectId, roleId));
                            }
                        }

                        processedUserIds.Add(userId); 
                    }
                    else
                    {
                        LogError(row.Worksheet.Name, row.RowNumber(), "Duplicate user id found - this row has been ignored.");
                    }
                }
                else
                {
                    LogError(row.Worksheet.Name, row.RowNumber(), "Could not create roles as user id was not found or created.");
                }
            }
        }

        private int GetUserIdFromUserName(IXLRow row, string userName)
        {
            int userId = -1;
            if (_userLookup.ContainsKey(userName))
            {
                userId = _userLookup[userName];
            }
            else
            {
                var userForename = row.Cell(USER_FORENAME_COLUMN_INDEX).Value.ToString();
                var userSurname = row.Cell(USER_SURNAME_COLUMN_INDEX).Value.ToString();
                var userEmail = row.Cell(USER_EMAIL_COLUMN_INDEX).Value.ToString();

                var result = CreateNewUser(userName, userForename, userSurname, userEmail);
                if (result.Success)
                {
                    userId = result.WrittenRecordKey.ID.Value;
                    logger.Info("Created new User : " + userName);
                    _userLookup.Add(userName, userId);
                }
                else
                {
                    LogError(row.Worksheet.Name, row.RowNumber(), string.Join(", ", result.Errors));
                }
            }
            return userId;
        }

        private WriteResult<User> CreateNewUser(string userName, string userForename, string userSurname, string userEmail)
        {
            var user = new User();
            user.Forename = userForename;
            user.Surname = userSurname;
            user.Email = userEmail;
            user.Enabled = true;
            user.UserName = userName.ToLower();

            return UserRepository.AddOrUpdateUser(user);
        }

        private int?[] GetRoleIdsForRoleColumns(IXLWorksheet ws)
        {
            var roleColumns = new List<int?>();

            var i = FIRST_ROLE_COLUMN_INDEX;
            var firstRowUsed = ws.FirstRowUsed();

            while (!firstRowUsed.Cell(i).IsEmpty())
            {
                var roleName = firstRowUsed.Cell(i).Value.ToString().ToUpper();

                if (_roleLookup.ContainsKey(roleName))
                {
                    roleColumns.Add(_roleLookup[roleName]);
                }
                else
                {
                    LogError(ws.Name, 0, "Role Code could not be found for " + roleName);
                    roleColumns.Add(null);
                }

                i++;
            }

            return roleColumns.ToArray();
        }

        #endregion
    }
}
