# README #

### What is this repository for? ###

The project security importer allows the upload of a spreadsheet with tabs for projects to upload. 
Each tab will contains a matrix of user ids and roles for upload. 

Its main use is for bulk loading the permissions for a project. 

### How do I get set up? ###

Just download it and run it up, there are some nuget packages referenced so you may need to refresh these before compiling. 

### Contribution guidelines ###

Usual AMEC guidelines apply. 
There are no unit tests yet but these should be added, however the importer relies on the DB this would really need to be mocked up for repeatable tests. 

### Who do I talk to? ###

Matt Wood was the creator but it's a pretty straight forward utility. 